$(document).ready(function(){
  checkCollapse();
});

function checkCollapse()
{
  var canSee = $('#navbar').is(":visible");
  if(canSee)
  {
      document.getElementById("main").style.paddingTop = "10px";
      document.getElementById("brand").classList.add("d-flex");
      document.getElementById("brand").classList.add("justify-content-center");
      document.getElementById("brand-text").innerHTML = "<b>Erik Tordera Bermejo</b>";
  }
  else
  {
    document.getElementById("main").style.paddingTop = "85px";
    document.getElementById("brand").classList.remove("d-flex");
    document.getElementById("brand").classList.remove("justify-content-center");
    document.getElementById("brand-text").innerHTML = "<b>Erik Tordera</b>";
  }

  resizeBody();
  positionateButtons();
}

function resizeBody()
{
  //alert(document.getElementById("global-navbar").clientHeight + "    " + window.innerHeight);
}

function positionateButtons()
{
  var canSee = $('#navbar').is(":visible");
  if(canSee)
  {
    document.getElementById("btn-menu-1").classList.remove("flex-row");
    document.getElementById("btn-menu-2").classList.remove("flex-row");
    document.getElementById("btn-menu-3").classList.remove("flex-row");
    document.getElementById("btn-menu-4").classList.remove("flex-row");
  }
  else
  {
      document.getElementById("btn-menu-1").classList.remove("flex-row-reverse");
      document.getElementById("btn-menu-2").classList.remove("flex-row-reverse");
      document.getElementById("btn-menu-3").classList.remove("flex-row-reverse");
      document.getElementById("btn-menu-4").classList.remove("flex-row-reverse");

      document.getElementById("btn-menu-1").classList.add("flex-row");
      document.getElementById("btn-menu-2").classList.add("flex-row");
      document.getElementById("btn-menu-3").classList.add("flex-row");
      document.getElementById("btn-menu-4").classList.add("flex-row");
  }

}
