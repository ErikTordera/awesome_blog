class EntriesController < ApplicationController
  layout "mainlayout"

  def new
    @entry = Entry.new
  end

  def create

    ap = article_params
    ap[:entry_type_id] = Integer(ap[:entry_type_id])
    ap[:user_id] = 1 #TODO CHANGE IF  WE HAVE MORE THAN 1 USER
    @entry = Entry.new(ap)

    if(@entry.save)
      redirect_to @entry
    else
      render 'new'
    end
  end

  def show
    @entry = Entry.find(params[:id])
  end

  private
  def article_params
    params.require(:entry).permit(:title, :abstract, :text, :link, :entry_type_id)
  end
end
