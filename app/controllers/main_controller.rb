class MainController < ApplicationController
  layout "mainlayout"
  def index
    @main_content =
    {
        "entries_type" => "all entries"
    }

    @entries = Entry.all().order("updated_at DESC")
  end

  def papers
    @main_content =
    {
        "entries_type" => "paper entries"
    }

    @entries = Entry.where(entry_type_id: "2")

    render 'index'
  end

  def first_impressions
    @main_content =
    {
        "entries_type" => "first impression entries"
    }

    @entries = Entry.where(entry_type_id: "1")

    render 'index'
  end

  def offtopics
    @main_content =
    {
        "entries_type" => "offtopic entries"
    }

    @entries = Entry.where(entry_type_id: "3")

    render 'index'
  end
end
