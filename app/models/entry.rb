class Entry < ApplicationRecord
  belongs_to :user
  has_many :comments, dependent: :destroy
  belongs_to :entry_type

  validates :title, presence: true,
                    length: { minimum: 5 }
  validates :entry_type, presence: true
end
