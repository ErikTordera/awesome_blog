Rails.application.routes.draw do
  get 'main/index'
  get 'main/papers' => 'main#papers', :as => :papers
  get 'main/impressions' => 'main#first_impressions', :as => :impressions
  get 'main/offtopics' => 'main#offtopics', :as => :offtopics


  resources :entries

  root 'main#index'
  # For details on the DSL available within this file, see http://guides.rubyonrails.org/routing.html
end
