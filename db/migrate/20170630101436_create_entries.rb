class CreateEntries < ActiveRecord::Migration[5.1]
  def change
    create_table :entries do |t|
      t.string :title
      t.text :text
      t.string :link
      t.string :image
      t.datetime :published_at

      t.belongs_to :author, index: true
      t.timestamps
    end
  end
end
