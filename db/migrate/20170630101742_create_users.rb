class CreateUsers < ActiveRecord::Migration[5.1]
  def change
    create_table :users do |t|
      t.string :name
      t.string :surname
      t.string :email
      t.text :password
      t.string :photo
      t.string :linkedin
      t.string :twitter
      t.string :facebook
      t.string :cv

      t.timestamps
    end
  end
end
