class CreateComments < ActiveRecord::Migration[5.1]
  def change
    create_table :comments do |t|
      t.text :text
      t.datetime :published_at

      t.belongs_to :entry, index: true
      t.timestamps
    end
  end
end
