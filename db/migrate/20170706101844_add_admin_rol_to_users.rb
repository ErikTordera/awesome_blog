class AddAdminRolToUsers < ActiveRecord::Migration[5.1]
  def change
    add_column :users, :admin_rol, :boolean
  end
end
